# iptables role for Ansible

## Variables

```yaml
iptables:
  script: iptables.sh # optional
```

If `script` is not specified then [default](files/iptables.sh) is used.
